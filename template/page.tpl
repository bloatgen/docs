{{define "page"}}<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"> 
    <title>{{$.title}} - {{$.sitename}}</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body>
    <div class="brand">Bloatgen Documentation</div>
    <nav>
      <ul>
        {{range $.nav}}<li><a href="./{{.path}}">{{.title}}</a></li>{{end}}
      </ul>
    </nav>
    <div class="wrap">
      {{$.content}}
    </div>
  </body>
</html>
{{end}}
