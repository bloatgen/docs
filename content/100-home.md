title: Home
template: page
filename: index.html
--
# What is bloatgen?
Bloatgen is a static site generator that focuses on simplicity, it is created
with the following goals in mind:

 * Simple templates that you can create with a single glance at an example if
   you've ever used html before.
 * The content of the static site is defined by a simple directory tree with
   html or markdown files.
 * Generating the website is a single command that outputs a directory with
   the resulting html files.

Bloatgen is not feature rich nor is it very powerful, but it does what it needs
to do: be simple enough to allow you to generate your own static pages right
away. Just looking at some of the examples is enough, **no need to read
documentation for basic pages**.

## License
Bloatgen and this documentation are released under the MIT license:
```
Copyright 2014-2015 clueless@thunked.org

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
```
