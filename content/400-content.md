title: Content
template: page
filename: content.html
--
# Creating content
Bloatgen content is a simple directory structure with content files. The way
files are treated depends on their extension.

## Content files: .html and .md
These files define the actual pages that you want to generate. All content files
must start with a key value map, followed by a line containing just ```--```.
Below the double dash goes the content and depending on the extension of the
file that is either html or markdown. The key value map at the top of a content
file will get passed into the template when generating the page for this content
file.

```
title: just a file to show the key value map
template: page
--
# Just a file to show the key value map
The template will be able to read the title variable using {{$.title}}. You
can't access the variables from the content though, the content is just regular
html or markdown.
```

## Variable files: .md.var and .var
These files get read but will not generate any output. These files can be
referenced in the key:value maps of content files. Files ending in .md.var have
their content parsed as markdown, other files are treated as plaintext. 

```
author: filevar:author_description.md.var
template: page
--
The template has access to the markdown parsed content of
/content/author_description.md.var is the {{$.author}} variable.
```

## Default key:value map: /CONFIG
In the root of the content directory you can have a file called CONFIG which
will contain default values for the key:value map of content files. Before a
content file is read its key:value map will be initialized with the variables in
the CONFIG file. This file is structured in the same way as content files, but
anything below the ```--``` is ignored.

```
siteroot: /
sitename: MyGreatSite
--
```

## Basic files: Everything else
All other files are treated as basic files and will just be copied to the output
directory.

## Key value map
A map with key:value pairs is passed into the template. Key value pairs are
parsed in the following way:

 1. Pairs are read line by line, they can not contain newlines.
 2. Pairs are split on the first colon (:) character. Keys can not contain
    colons, values can.
 3. Both keys and values have leading and trailing spaces removed after
    splitting.
 4. Some values undergo additional parsing.

Each content file has its key:value map initialized with all the values defined
in the CONFIG file. If a content file specifies the same keys, they take
precedence over the CONFIG file's keys.

### Keys that have a non standard behavior
Some key names cause special behavior or are reserved.

#### template
This defines the name of the named template that will be used to generate this
content. This key is **mandatory**.

#### filename
If this key is specified its value defines the output filename for this content
file. If omitted the output filename will be the same as the content filename
with .md changed to .html. This filename is not a path, just a filename.


#### content
This key is **reserved** and should not be specified in content or CONFIG files.
Bloatgen will set this value to the main content. Templates should use this key
to print the main content.

### Value parsing
For the most part values are just lines of text. However under some conditions
values are parsed in a more complex way that allows you to specify function
calls. This exposes extra functionality to the content creator.

Essentially values are parsed if they look like a function call or a string
literal. Function calls start with a name and are followed by an opening
parenthesis. String literals are enclosed in ' " or `

```
a: this is just a normal line of text (really, it is!)
b: (another "normal" line)
c: function(call("this line is parsed"))
d: "a literal that gets parsed" this text after the literal is a syntax error
e: "function() looks like a function but it's contained in this literal: not parsed!"
f: 'in a literal repeating the enclosing character ('') twice causes it to be escaped and threated as 1'
h: "that works "" and ` too"
```

For more information on how the *"language"* is parsed please take a look at
[the
grammar](https://gitlab.com/bloatgen/bloatgen/wikis/grammar-key-value-map).


#### list(*strlist*)
List takes a comma separated value list as a string and returns an array
containing these values. Leading and trailing whitespace is removed in the
output.
```
tags: list("documentation, just an example, bloatgen")
```

#### json(*strdata*)
Json takes a string containing a json encoded data structure as a string and
returns the decoded data structure.
```
data: json(`{"weight":10,"name":"something"}`)
jsontags: json(`["some","tags","etc"]`)
```

#### filevar(*strvarpath*)
filevar takes the relative path of a .var file and returns the content of that
file. If the .var file is a .md.var file the content is first processed as
markdown. It can only reference files inside the content directory.
```
author: filevar("author_description.md.var")
```

#### content(*pattern*, ...)
Content returns a list of output files and associated variables. Its first
argument is a [file matching
pattern](http://golang.org/pkg/path/filepath/#Match) that defines which output
files are included in the list. The matched file path will be available in the
```path``` key of the returned list of files.

All subsequent arguments are optional and each argument defines one value that
will be copied from the content file to the returned list.

For example, to create a list of all output files in the root directory along
with their title variable:
```
nav: content("*.html", "title")
```

a template file can then access this information to show a menu like so:
```
<ul>
    {{range $item := $.nav}}
        <li><a href="{{$item.path}}">$item.title</a></li>
    {{end}}
</ul>
```
