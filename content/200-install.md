title: Install
template: page
filename: install.html
--
# Installing bloatgen
_note: this documentation assumes you have go installed and properly configured, please read [the go installation manual](http://golang.org/doc/install) if that is not the case. Specifically, your GOPATH needs to be correctly set up._

Assuming you have your go environment set up properly installing bloatgen will only take a second. Just run the following two commands:
```
$ go get gitlab.com/bloatgen/bloatgen
$ go install gitlab.com/bloatgen/bloatgen
```

You can also clone the [examples repository](https://gitlab.com/bloatgen/examples) and build the minimal example. **Keep in mind that the content of the output directory is recursively deleted before rendering**.
```
$ git clone https://gitlab.com/bloatgen/examples.git
$ cd example
$ bloatgen minimal/content minimal/template output/
```

Finally, you can grab and build the documentation locally using bloatgen. The resulting documentation will be put in the output directory. **Keep in mind that the content of the output directory is recursively deleted before rendering**.
```
$ git clone https://gitlab.com/bloatgen/docs.git
$ cd docs
$ bloatgen content/ template/ output/
```
