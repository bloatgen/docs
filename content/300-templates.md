title: Templates
template: page
filename: templates.html
--
# Creating templates
A bloatgen template is a simple directory that contains two things: a
subdirectory with static assets (css, js, images, etc) and some [go html
templates](http://golang.org/pkg/html/template/). The name of the template
directory can be anything, but in this document it will be ```/template/```.

## Example template directory
The following example template directory structure will be used throughout the
document to explain template behavior.
```
/template/
/template/README
/template/blogpost.tpl
/template/basicpage.tpl
/template/assets/
/template/assets/css/
/template/assets/css/style.css
```

## Static Assets
All static assets go in a subdirectory ```/template/assets/```. When generating
a website the complete contents of the assets directory will be recursively
copied into the output directory. Any symbolic links will be re-created as a
symbolic link, not as a file.

Given the above example template directory structure the output directory will
contain the following files after initializing it (before the content is
parsed).
```
/output/
/output/css/
/output/css/style.css
```

## Template files
All files that match ```/template/*.tpl``` will be parsed as a single go
template with named subtemplates. You don't need to know a lot about go
templates to get started with bloatgen, the basics are explained below. For
more advanced use of go templates you can read the go
[text/template](http://golang.org/pkg/text/template/) and
[html/template](http://golang.org/pkg/html/template/) documentation.

Template "actions" are enclosed by double curly braces. Templates have access
to some data that is passed to them. The root data variable is ```$```. All
variables passed to the template are defined by the content files. There is only
one variable that is guaranteed to exist: ```content``` contains the parsed
content of a .html or .md content file.

Defining a named template ```basicpage``` that just prints the contents of one
of the content files:
```
{{define "basicpage"}}
<!DOCTYPE html>
<html>
  <body>
    {{$.content}}
  </body>
</html>
{{end}}
```

Using ```{{template}}``` you can include other partial templates. You can
include partial templates from the same .tpl file or from other .tpl files. All
named subtemplates exist in the same namespace.
```
{{define "top"}}
    <h1>{{$.title}}</h1>
{{end}}

{{define "basicpage"}}
    {{template "top"}}
    <p>
        {{$.content}}
    </p>
{{end}}
```

Finally you can easily loop over a list using ```{{range}}```
```
{{define "blogpost"}}
    <ul>
    {{range $tag := $.tags}}
        <li>{{$tag}}</li>
    {{end}}
    </ul>
{{end}}
```
